using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;

public class Bot
{
    public const int TICKS_PER_SECOND = 60;
    public const double DEBUG_THROTTLE = 0.6;
    public const double DEFAULT_C = 0.00710531449539186;

    private StreamWriter writer;
    private yourCar MyCar { get; set; }
    private gameInit GameInit { get; set; }

    public PiecePosition CurrentPos { get; set; }
    public PiecePosition PreviousPos { get; set; }
    public PiecePosition Tick1 { get; set; }

    public int? MyCarIndex { get; set; }
    public double? MyCarAngle { get; set; }

    public int CurvesMaxIndex { get; set; }
    public List<Piece> Curves { get; set; }

    public int SwitchesMaxIndex { get; set; }
    public List<SwitchAux> Switches { get; set; }

    public int CurrentBestLane { get; set; }

    public int GameTick { get; set; }

    public PiecePosition CrashPos { get; set; }

    /// <summary>
    /// Drag Constant
    /// </summary>
    public double K { get; set; }

    public double Mass { get; set; }

    private double? _cf;

    public double CalcCf()
    {
        var v = 6.7;
        var lane = CurrentPos.Lane.EndLaneIndex;
        if (CrashPos != null)
        {
            v = CrashPos.Velocity;
            lane = CrashPos.Lane.EndLaneIndex;
        }

        var res = Math.Pow(v, 2.0) * GameInit.Data.Race.Track.Pieces[Curves[0].ExIndex.Value].LaneRadius[lane];
        return res;
    }

    public static void Main(string[] args)
    {
        string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

        Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        using (TcpClient client = new TcpClient(host, port))
        {
            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);
            StreamWriter writer = new StreamWriter(stream);
            writer.AutoFlush = true;

            new Bot(reader, writer, new Join(botName, botKey));
        }
    }

    
    public void Init()
    {
        Utils.Init();
        //Cf = DEFAULT_C;
        Mass = -1;
    }

    public createRace CreateRace()
    {
        createRace cr = new createRace()
        {
            MsgType = "createRace",
            Data = new DataCr()
            {
                BotId = new BotId() { Name = "likeAphoenix", Key = "dKfWp0EDSuvTSw" },
                //TrackName = "keimola",
                TrackName = "usa",
                //TrackName = "germany",
                Password = "phoenix",
                CarCount = 1
            }
        };
        return cr;

        //Finland (keimola)
        //Germany (germany)
        //U.S.A. (usa)

    }

    Bot(StreamReader reader, StreamWriter writer, Join join)
    {
        this.writer = writer;
        string line;

        Init();
        send(join);

        //int? bestLane = null;
        while ((line = reader.ReadLine()) != null)
        {
            MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
            switch (msg.msgType)
            {
                case "createRace":
                    var cr = CreateRace().Serialize();
                    send(cr);
                    break;
                case "yourCar":
                    MyCar = ParseYourCar(line);
                    Console.WriteLine("Your Car");
                    send(new Ping());
                    break;
                case "carPositions":
                    var cp = ParseCarPositions(line);
                    var action = DoThrottleAi();
                    action.GameTick = GameTick;

                    send(action);
                    break;
                case "join":
                    Console.WriteLine("Joined");
                    send(new Ping());
                    break;
                case "gameInit":
                    GameInit = ParseGameInit(line);

                    Console.WriteLine("Race init");
                    send(new Ping());
                    break;
                case "gameEnd":
                    Console.WriteLine("Race ended");
                    send(new Ping());
                    break;
                case "gameStart":
                    Console.WriteLine("Race starts");
                    send(new Ping());
                    break;
                case "crash":
                    CrashPos = PreviousPos;
                    Console.WriteLine("CRASH! CentriForce: '{0}'", CalcCf());
                    send(new Ping());
                    break;
                default:
                    send(new Ping());
                    break;
            }

            string text = line != null ? line : "";
            //text = string.Format("{0}. bestLane:{1}", text, bestLane);
            Utils.SaveFile(Utils.RaceLogFileName, true, text);
        }
    }

    #region Parse Methods
    public carPositions ParseCarPositions(string line)
    {
        var cp = carPositions.FromJson(line);
        PreviousPos = CurrentPos;
        CurrentPos = cp.Data[MyCarIndex.Value].PiecePosition;
        CurrentPos.ExtSlipAngle = cp.Data[MyCarIndex.Value].Angle;
        
        GameTick = cp.GameTick;
        return cp;
    }

    public gameInit ParseGameInit(string line)
    {
        var gi = gameInit.FromJson(line);

        MyCarAngle = gi.Data.Race.Track.StartingPoint.Angle;

        #region Find My Car Index
        for (int i = 0; i < gi.Data.Race.Cars.Length; i++)
        {
            var car = gi.Data.Race.Cars;
            if (car[i].Id.Name == MyCar.Data.Name
                && car[i].Id.Color == MyCar.Data.Color)
            {
                MyCarIndex = i;
                break;
            }
        }
        #endregion

        #region Cache Curves + Cache Switches
        for (int i = 0; i < gi.Data.Race.Track.Pieces.Length; i++)
        {
            //var currPiece = gi.Data.Race.Track.Pieces[i];
            var piece = gi.Data.Race.Track.Pieces[i];
            //piece.Distance = CalcDistance(piece);
            if (i == 0)
            {
                Curves = new List<Piece>();
                Switches = new List<SwitchAux>();
                //Switches = new List<SwitchAux>() { new SwitchAux() { Index = 13 } };
            }

            for (int j = 0; j < gi.Data.Race.Track.Lanes.Length; j++)
            {
                if (j == 0)
                {
                    piece.DistanceFromLastSwitch = new double[gi.Data.Race.Track.Lanes.Length];
                    piece.Distance = new double[gi.Data.Race.Track.Lanes.Length];
                    piece.LaneRadius = new double[gi.Data.Race.Track.Lanes.Length];
                }

                //else
                {
                    var lane = gi.Data.Race.Track.Lanes[j];
                    if (piece.Angle.HasValue)
                    {
                        int signal = piece.Angle >= 0 ? -1 : 1;
                        int radius = lane.DistanceFromCenter * signal + piece.Radius.Value;
                        piece.Distance[j] = Math.Abs(ArcValue(radius, piece.Angle.Value));
                        piece.LaneRadius[j] = radius;
                    }
                    else
                    {
                        piece.Distance[j] = piece.Length;
                    }

                    var prevDistanceFromLastSwitch = i == 0 ? 0 : gi.Data.Race.Track.Pieces[i - 1].DistanceFromLastSwitch[j] + gi.Data.Race.Track.Pieces[i - 1].Distance[j];
                    var prevPieceIsSwitch = i == 0 ? false : gi.Data.Race.Track.Pieces[i - 1].Switch.HasValue && gi.Data.Race.Track.Pieces[i - 1].Switch.Value;
                    var distanceFromLastSwitch = prevPieceIsSwitch ? piece.Distance[j] : prevDistanceFromLastSwitch;
                    piece.DistanceFromLastSwitch[j] = distanceFromLastSwitch;
                }
            }
            piece.ExIndex = i;
            if (piece.Angle.HasValue)
            {
                double prevCarAngle = Curves.Count == 0 ? MyCarAngle.Value : Curves[Curves.Count - 1].CarAngleAtPiece.Value;

                //if it's first piece OR if last piece was a segment!
                piece.IsCurveBeginning = (i == 0) || (!gi.Data.Race.Track.Pieces[i - 1].Angle.HasValue);

                piece.CarAngleAtPiece = AddAngle(prevCarAngle, piece.Angle);
                Curves.Add(piece);
                CurvesMaxIndex = i;
            }

            if (piece.Switch.HasValue && piece.Switch.Value)
            {
                Switches.Add(new SwitchAux() { Index = i });
                SwitchesMaxIndex = i;
            }
        }

        //recalculate distances from last switch until first switch
        if (Switches.Count > 1)
        {
            var lastPiece = gi.Data.Race.Track.Pieces[gi.Data.Race.Track.Pieces.Length - 1].ExIndex.Value;
            for (int i = 0; i < Switches[0].Index; i++)
            {
                var piece = gi.Data.Race.Track.Pieces[i];
                for (int j = 0; j < gi.Data.Race.Track.Lanes.Length; j++)
                {
                    var prevIndex = GetPrevIndex(i, lastPiece);
                    var prevDistanceFromLastSwitch = gi.Data.Race.Track.Pieces[prevIndex].DistanceFromLastSwitch[j] + gi.Data.Race.Track.Pieces[prevIndex].Distance[j];
                    var prevPieceIsSwitch = gi.Data.Race.Track.Pieces[GetPrevIndex(prevIndex, lastPiece)].Switch.HasValue && gi.Data.Race.Track.Pieces[GetPrevIndex(prevIndex, lastPiece)].Switch.Value;
                    var distanceFromLastSwitch = prevPieceIsSwitch ? piece.Distance[j] : prevDistanceFromLastSwitch;
                    piece.DistanceFromLastSwitch[j] = distanceFromLastSwitch;
                }
            }
        }

        #endregion
        return gi;
    }

    private int GetPrevIndex(int i, int max)
    {
        return i - 1 < 0 ? max : i - 1;
    }

    public double[] CalcDistance(Piece p)
    {
        for (int i = 0; i < GameInit.Data.Race.Track.Lanes.Length; i++)
        {
            var lane = GameInit.Data.Race.Track.Lanes[i];
            if (p.Angle.HasValue)
            {
                double radius = lane.DistanceFromCenter * -1 + p.Length;
                p.Distance[i] = Math.Abs(ArcValue(radius, p.Angle.Value));
            }
            else
            {
                p.Distance[i] = p.Length;
            }
        }

        return p.Distance;
    }

    public double ArcValue(double radius, double angle)
    {
        return angle * Math.PI * radius / 180;
        //return CirclePerimetre(radius) / angle;
    }

    public double CirclePerimetre(double radius)
    {
        return 2 * Math.PI * radius;
    }

    public yourCar ParseYourCar(string line)
    {
        return yourCar.FromJson(line);
    }
    #endregion

    public SendMsg DoThrottleAi()
    {
        string velMsg2 = "";
        bool isStartGame = (PreviousPos == null);
        SendMsg switchLane = null;

        int currentPieceIndex = CurrentPos.PieceIndex;

        if (HasPieceChanged())
        {
            var pieceAngle = GameInit.Data.Race.Track.Pieces[CurrentPos.PieceIndex].Angle;
            MyCarAngle = AddAngle(MyCarAngle.Value, pieceAngle);

            Switches = Utils.ShiftListSwitchs(currentPieceIndex, Switches, SwitchesMaxIndex);
            var aux = Curves[0];
            Curves = Utils.ShiftListPieces(currentPieceIndex, Curves, Switches[0].Index, CurvesMaxIndex);

            if (aux.ExIndex != Curves[0].ExIndex)
            {
                //CalcMaxCentrifugalForce();
            }

            //CurrentBestLane = GetBestLaneDebug02();
            CurrentBestLane = GetBestLane();
            if (//GameInit.Data.Race.Track.Pieces[CurrentPos.PieceIndex].HasSwitch() &&
                CurrentBestLane != CurrentPos.Lane.EndLaneIndex)
            {
                switchLane = SwitchToBestLane(CurrentBestLane);
            }

            if (currentPieceIndex == Switches[0].Index)
            {
                //debug
            }

            Console.WriteLine("Piece: " + currentPieceIndex);

            var secondSwitch = Switches.Count > 1 ? 1 : 0;
            var prevToNextSwitchIndex = GetPrevIndex(Switches[secondSwitch].Index, GameInit.Data.Race.Track.Pieces.Length);
            var prevToNextSwitch = GameInit.Data.Race.Track.Pieces[prevToNextSwitchIndex];
            var item = GameInit.Data.Race.Track.Pieces[currentPieceIndex];
            var excelText2 = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}", Utils.PutQuotes(item.ExIndex), Utils.PutQuotes(item.Distance[0]),
            Utils.PutQuotes(item.Distance[1]), Utils.PutQuotes(item.DistanceFromLastSwitch[0]), Utils.PutQuotes(item.DistanceFromLastSwitch[1]),
            Utils.PutQuotes(CurrentBestLane), Utils.PutQuotes(CurrentPos.Lane.EndLaneIndex)
            , prevToNextSwitch.ExIndex//index of next switch-1;
            , prevToNextSwitch.DistanceFromLastSwitch[0] //cost to nextSwitch-1; lane 0
            , prevToNextSwitch.DistanceFromLastSwitch[1] //cost to nextSwitch-1; lane 1
            , Switches[0].Index == currentPieceIndex
            );
            Utils.SaveFile(Utils.ExcelFileName, true, excelText2);
        }



        if (HasLaneChanged())
        {

        }

        double? distanceToNextCurve = null;
        double? velocityNeededInCurve = null;
        double? ticksToAchieveDistance = null;
        double? ticksToAchieveVelocityNextCurve = null;

        if (PreviousPos != null && GameTick > 0)
        {

            CurrentPos.TraveledDistance = TraveledDistance(PreviousPos, CurrentPos);
            CurrentPos.Acceleration = CurrentPos.TraveledDistance / GameTick;
            CurrentPos.Velocity = CurrentPos.TraveledDistance;



            if (PreviousPos.Velocity > 0.1 && K <= 0)
            {
                CalcDragConstant();
            }

            if (Mass.CompareTo(0) < 0)
            {
                CalcMass();
            }

            if (K > 0)
            {
                var nextCurve = GameInit.Data.Race.Track.Pieces[Curves[0].ExIndex.Value];
                var v0 = CurrentPos.Velocity == 0.0 ? 1 : CurrentPos.Velocity;

                distanceToNextCurve = TraveledDistance(CurrentPos, nextCurve.ExIndex.Value);

                velocityNeededInCurve = CalcCurveVelocity(nextCurve.LaneRadius[CurrentPos.Lane.EndLaneIndex]) ;
                ticksToAchieveDistance = CalcTicksToDistance(distanceToNextCurve.Value);
                ticksToAchieveVelocityNextCurve = CalcTicksNeededToVelocity(velocityNeededInCurve.Value);

                var velMsg = string.Format("Tick:{6};Piece:{7}; distToCurve:'{0}'; velInCurve:'{1}';currVel:'{4}'; ticksToDist:'{2}'; ticksToVelNextCurve:{3}; slip:'{5}'; a:{8}",
                    distanceToNextCurve,
                    velocityNeededInCurve,
                    ticksToAchieveDistance,
                    ticksToAchieveVelocityNextCurve,
                    CurrentPos.Velocity,
                    CurrentPos.ExtSlipAngle,
                    GameTick,
                    CurrentPos.PieceIndex,
                    CurrentPos.Acceleration);
                
                velMsg2 = velMsg;
                //Console.WriteLine(velMsg);
                Utils.SaveFile(Utils.VelocityFileName, true, velMsg);
            }
        }


        //if it's game start, send throttle 1
        if (GameTick <= 3 || PreviousPos.Velocity < 0.1)
        {

            var res = new Throttle(1.0);
            CurrentPos.ExtThrottle = res.value;
            return res;
        }
        else
        {
            //DEBUG:
            //if (CurrentPos.Lap > 0 && CurrentPos.PieceIndex >= 12)
            //{
            //    //Console.WriteLine("BREAK!");
            //    var res = new Throttle(1.0);
            //    CurrentPos.ExtThrottle = res.value;
            //    return res;
            //}


            if (switchLane != null && CurrentPos.Lane.EndLaneIndex == CurrentPos.Lane.StartLaneIndex)
            {
                Console.WriteLine("CurrPiece:{0}; SwitchTo:{2}; FromLane:{1}; NewLane: {3}", currentPieceIndex, CurrentPos.Lane.StartLaneIndex, CurrentPos.Lane.EndLaneIndex, ((SwitchLane)switchLane).value);
                Console.WriteLine(velMsg2);
                CurrentPos.ExtThrottle = PreviousPos.ExtThrottle;
                return switchLane;
            }
            else
            {
                var res = new Throttle(PreviousPos.ExtThrottle);
                //var res = new Throttle(DEBUG_THROTTLE);
                
                //if (ticksToAchieveVelocityNextCurve <= ticksToAchieveDistance)
                //{
                //    if (CurrentPos.ExtSlipAngle > 0 || distanceToNextCurve <= 50)
                //    {
                //        res = new Throttle(0.4);
                //    }
                //    else
                //    {
                //        double newThrottle = CalcThrottleByVelocity(velocityNeededInCurve.Value);
                //        res = new Throttle(newThrottle);
                //    }
                //}
                //else
                //{
                //    res = new Throttle(1.0);
                //}
                //CurrentPos.ExtThrottle = res.value;
                //return res;

                if (CurrentPos.ExtSlipAngle > 0 || distanceToNextCurve <= 50)
                {
                    res = new Throttle(0.4);
                }
                else
                {
                    double newThrottle = CalcThrottleByVelocity(velocityNeededInCurve.Value);
                    //res = new Throttle(newThrottle);
                    res = new Throttle(1.0);
                }
            }
        }
        var res2 = new Throttle(DEBUG_THROTTLE);
        CurrentPos.ExtThrottle = res2.value;
        return res2;
    }

    public SendMsg SwitchToBestLane(int? bestLane = null)
    {
        if (!bestLane.HasValue)
        {
            bestLane = GetBestLane();
        }

        if (bestLane > CurrentPos.Lane.EndLaneIndex)
        {
            return new SwitchLane("Right");
        }
        else if (bestLane < CurrentPos.Lane.EndLaneIndex)
        {
            return new SwitchLane("Left");
        }
        return null;
    }

    public bool LeftToRightDirection(double angle)
    {
        return angle >= 90 && angle <= 270;
    }

    public bool HasLaneChanged()
    {
        if (PreviousPos == null && CurrentPos == null)
            return false;

        if (PreviousPos == null && CurrentPos != null)
            return true;

        return PreviousPos.Lane.StartLaneIndex != CurrentPos.Lane.StartLaneIndex;
    }

    public bool HasPieceChanged()
    {
        if (PreviousPos == null && CurrentPos == null)
            return false;

        if (PreviousPos == null && CurrentPos != null)
            return true;

        return PreviousPos.PieceIndex != CurrentPos.PieceIndex;
    }

    public double AddAngle(double source, double? newAngle)
    {
        if (newAngle.HasValue)
        {
            source += newAngle.Value;
            if (source >= 360)
            {
                source = source - 360;
            }
        }
        return source;
    }

    public int GetBestLane()
    {
        int lane = CurrentPos.Lane.EndLaneIndex;
        double min = double.MaxValue;
        bool tie = false;
        int maxPieces = GameInit.Data.Race.Track.Pieces.Length;
        for (int i = 0; i < GameInit.Data.Race.Track.Lanes.Length; i++)
        {
            if (Switches.Count > 0)
            {
                int secondSwitch;
                if (Switches.Count > 1)
                {
                    secondSwitch = 1;
                }
                else
                {
                    secondSwitch = 0;
                }

                var distance = GameInit.Data.Race.Track.Pieces[GetPrevIndex(Switches[secondSwitch].Index, maxPieces)].DistanceFromLastSwitch[i];
                tie = min == distance;
                if (distance < min)
                {
                    min = distance;
                    lane = GameInit.Data.Race.Track.Lanes[i].Index;
                    tie = false;
                }
            }
        }

        if (tie)
        {
            //lane = GetBestLane_CurveEuristic();
            lane = CurrentPos.Lane.EndLaneIndex;
        }
        return lane;
    }

    public int GetBestLaneSimpleMax()
    {
        int lane = CurrentPos.Lane.EndLaneIndex;
        double min = double.MaxValue;
        bool tie = false;
        for (int i = 0; i < GameInit.Data.Race.Track.Lanes.Length; i++)
        {
            if (Switches.Count > 0)
            {
                var distance = GameInit.Data.Race.Track.Pieces[Switches[0].Index].DistanceFromLastSwitch[i];
                tie = min == distance;
                if (distance < min)
                {
                    min = distance;
                    lane = GameInit.Data.Race.Track.Lanes[i].Index;
                    tie = false;
                }
            }
        }

        if (tie)
        {
            //lane = GetBestLane_CurveEuristic();
        }
        return lane;
    }

    /// <summary>
    /// k = ( v(1) - ( v(2) - v(1) ) ) / v(1)2 * h;
    /// h = throttle
    /// </summary>
    /// <returns></returns>
    public void CalcDragConstant()
    {
        //var v1 = Tick1.Velocity;
        //var v2 = PreviousPos.Velocity;

        var v1 = PreviousPos.Velocity;
        var v2 = CurrentPos.Velocity;

        double throttle = 1.0;
        K = (v1 - (v2 - v1)) / Math.Pow(v1, 2.0) * throttle;
    }


    //public double CalcDragConstant()
    //{
    //    //var v1 = Tick1.Velocity;
    //    //var v2 = PreviousPos.Velocity;

    //    var v1 = PreviousPos.Velocity;
    //    var v2 = CurrentPos.Velocity;

    //    double throttle = 1.0;
    //    return (v1 - (v2 - v1)) / Math.Pow(v1, 2.0) * throttle;
    //}

    /// <summary>
    /// m = 1.0 / ( ln( ( v(3) - ( h / k ) ) / ( v(2) - ( h / k ) ) ) / ( -k ) )
    /// </summary>
    public void CalcMass()
    {
        var v2 = PreviousPos.Velocity;
        var v3 = CurrentPos.Velocity;
        var h = 1;

        Mass = 1.0 / ((Math.Log((v3 - (h / K)) / (v2 - (h / K)))) / (-K));
    }

    /// <summary>
    /// v(t) = (v(0) - (h/k) ) * e^ ( ( - k * t ) / m ) + ( h/k )
    /// </summary>
    /// <param name="gameTick"></param>
    /// <returns></returns>
    public double CalcVelocityInTick(int tick, double throttle)
    {
        var v0 = CurrentPos.Velocity;
        var h = throttle;
        var t = tick;
        var m = Mass;
        var v = (v0 - (h/K)) * Math.Exp( (-K * t)/m) + (h/K);
        return v;
    }

    /// <summary>
    /// How many ticks needed to reach Velocity X
    /// t = ( ln ( (v - ( h/k ) )/(v(0) - ( h/k ) ) ) * m ) / ( -k )
    /// </summary>
    /// <param name="velocity"></param>
    /// <returns></returns>
    public int CalcTicksNeededToVelocity(double velocity)
    {
        var v = velocity;
        var v0 = CurrentPos.Velocity == 0.0 ? 1 : CurrentPos.Velocity;
        var h = CurrentPos.ExtThrottle;//CalcThrottleByVelocity(v0);


        double logAux = (((v - (h / K))) / (v0 - (h / K))) * Mass;
        var res = Math.Log(logAux) / (-K);
        return (int)Math.Ceiling(Math.Abs(res));
    }

    public double CalcThrottleByVelocity(double velocity)
    {
        return K * velocity;
    }

    public double CalcCurveVelocity(double curveLaneRadius)
    {
        var c = CalcCf();
        return Math.Sqrt(c * curveLaneRadius);        
    }

    public int CalcTicksToDistance(double distance)
    {
        var v0 = CurrentPos.Velocity;
        var d = distance;
        var a = CurrentPos.Acceleration;
        var t = Math.Sqrt(((d - v0) * 2) / a);
        return (int)Math.Ceiling(t);
    }

    /// <summary>
    /// Distance travelled in X ticks
    /// d(t) = ( m/k ) * ( v(0) - ( h/k ) ) * ( 1.0 - e^ ( ( -k*t ) / m ) ) + ( h/k ) * t + d(0)
    /// </summary>
    /// <param name="ticks"></param>
    /// <returns></returns>
    public double CalcDistanceTraveledInTicks(int ticks)
    {
        var m = Mass;
        var v0 = CurrentPos.Velocity;
        var h = CalcThrottleByVelocity(v0);
        var t = ticks;
        var d0 = 0.0;
        
        var res = (m / K) * (v0 - (h / K)) * (1.0 - Math.Exp((-K * t) / m)) + (h / K) * t + d0;
        return res;
    }

    //public int GetBestLaneDebug01()
    //{
    //    var i = CurrentPos.PieceIndex;
    //    if (i <= 3) return 1;
    //    else if (i <= 8) return 1;
    //    else if (i <= 13) return 0;
    //    else if (i <= 18) return 0;
    //    else if (i <= 25) return 1;
    //    else if (i <= 29) return 1;
    //    else if (i <= 35) return 1;

    //    return CurrentPos.Lane.EndLaneIndex;
    //}

    ////Winner
    //public int GetBestLaneDebug02()
    //{
    //    var i = CurrentPos.PieceIndex;
    //    if (i <= 3) return 1;
    //    else if (i <= 8) return 0;
    //    else if (i <= 13) return 0;
    //    else if (i <= 18) return 1;
    //    else if (i <= 25) return 1;
    //    else if (i <= 29) return 1;
    //    else if (i <= 35) return 1;

    //    return CurrentPos.Lane.EndLaneIndex;
    //}

    [Obsolete]
    public int GetBestLaneOld(int? fromPiece = null)
    {
        var lanes = GameInit.Data.Race.Track.Lanes;
        if (lanes.Length == 1)
            return lanes[0].Index;

        if (Curves.Count > 0)
        {
            Piece nextCurve;
            if (fromPiece.HasValue)
            {
                nextCurve = Curves[fromPiece.Value];
            }
            else
            {
                nextCurve = Curves[0];
            }

            if (nextCurve.Angle > 0)
            {
                return lanes[lanes.Length - 1].Index;
            }
            else
            {
                return lanes[0].Index;
            }

        }
        return 0;
    }

    private void send(SendMsg msg)
    {
        var text = msg.ToJson();
        send(text);
    }

    private void send(string text)
    {
        writer.WriteLine(text);
        Utils.SaveFile(Utils.SentLogFileName, true, text);

        //string velHeader = "LastPiece, CurrPiece, PrevInPieceDist, CurrInPieceDist, TraveledDistance, GameTick, Velocity";
        if (PreviousPos != null && GameTick > 0)
        {
            //var debug01 = PreviousPos;var debug02 = CurrentPos; debug01.PieceIndex = 0;debug01.InPieceDistance = 90.0;debug02.PieceIndex = 2;debug02.InPieceDistance = 50.0;var debug = TraveledDistance(debug01, debug02);
            string velMsg = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}",
                PreviousPos.PieceIndex,
                CurrentPos.PieceIndex,
                PreviousPos.InPieceDistance,
                CurrentPos.InPieceDistance,
                CurrentPos.TraveledDistance,
                GameTick,
                CurrentPos.Velocity,
                CurrentPos.ExtSlipAngle);
            //Utils.SaveFile(Utils.VelocityFileName, true, velMsg);
        }
    }


    private double TraveledDistance(PiecePosition from, int toIndex)
    {
        var endLane = from.Lane.EndLaneIndex;
        var to = new PiecePosition()
        {
            PieceIndex = toIndex,
            InPieceDistance = 0,
            Lane = new Lane() { StartLaneIndex = endLane, EndLaneIndex = endLane }
        };
        return TraveledDistance(from, to);
    }

    private double TraveledDistance(PiecePosition from, PiecePosition to)
    {
        double totalDist = 0.0;
        for (int i = from.PieceIndex; i <= to.PieceIndex; i++)
        {
            var piece = GameInit.Data.Race.Track.Pieces[i];

            if (from.PieceIndex == to.PieceIndex)
            {
                return to.InPieceDistance - from.InPieceDistance;
            }
            else if (to.PieceIndex == i)
            {
                totalDist += to.InPieceDistance;
            }
            else if (from.PieceIndex != i && to.PieceIndex != i)
            {
                totalDist += piece.Distance[from.Lane.EndLaneIndex];
            }
            else
            {
                totalDist += piece.Distance[from.Lane.StartLaneIndex] - from.InPieceDistance;
            }
        }
        return totalDist;
    }
}

public class MsgWrapper
{
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data)
    {
        this.msgType = msgType;
        this.data = data;
    }
}

public abstract class SendMsg
{
    [JsonProperty("gameTick", NullValueHandling = NullValueHandling.Ignore)]
    public object GameTick;

    //public SendMsg(){ }

    //public SendMsg(int gameTick)
    //{
    //    this.GameTick = gameTick;    
    //}

    public string ToJson()
    {
        return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
    }
    protected virtual Object MsgData()
    {
        return this;
    }

    protected abstract string MsgType();
}

public class Join : SendMsg
{
    public string name;
    public string key;
    public string color;
    public string trackName;

    public Join(string name, string key)
    {
        this.name = name;
        this.key = key;
        this.color = "red";
        //this.trackName = "usa";
    }

    protected override string MsgType()
    {
        return "join";
    }
}

public class Ping : SendMsg
{
    protected override string MsgType()
    {
        return "ping";
    }
}

public class Throttle : SendMsg
{
    public double value;

    public Throttle(double value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "throttle";
    }
}

public class SwitchLane : SendMsg
{
    public string value;

    public SwitchLane(string value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "switchLane";
    }
}

#region CAR POSITIONS
[Serializable]
public class carPositions
{

    public string MsgType;
    public Datum[] Data;
    public string GameId;
    public int GameTick;

    //Empty Constructor
    public carPositions() { }

    public string Serialize()
    {
        return JsonConvert.SerializeObject(this);
    }
    public static carPositions FromJson(string json)
    {
        return JsonConvert.DeserializeObject<carPositions>(json);
    }
}


[Serializable]
public class Id
{

    public string Name;
    public string Color;

    //Empty Constructor
    public Id() { }

}


[Serializable]
public class Lane
{

    public int StartLaneIndex;
    public int EndLaneIndex;

    //Empty Constructor
    public Lane() { }

}


[Serializable]
public class PiecePosition
{
    public double ExtThrottle { get; set; }
    public double ExtSlipAngle { get; set; }
    public double Acceleration { get; set; }
    public double Velocity { get; set; }
    public double TraveledDistance { get; set; }

    public int PieceIndex;
    public double InPieceDistance;
    public Lane Lane;
    public int Lap;

    //Empty Constructor
    public PiecePosition() { }

}


[Serializable]
public class Datum
{

    public Id Id;
    public double Angle;
    public PiecePosition PiecePosition;

    //Empty Constructor
    public Datum() { }

}
#endregion

#region GAME INIT
[Serializable]
public class gameInit
{
    //public int ExtTrackWidth { get; set; }


    public string MsgType;
    public DataGi Data;

    //Empty Constructor
    public gameInit() { }

    public string Serialize()
    {
        return JsonConvert.SerializeObject(this);
    }
    public static gameInit FromJson(string json)
    {
        return JsonConvert.DeserializeObject<gameInit>(json);
    }
}


[Serializable]
public class Piece
{
    public double[] DistanceFromLastSwitch { get; set; }
    public double[] Distance { get; set; }
    public double[] LaneRadius { get; set; }

    public int? ExIndex { get; set; }
    public double? CarAngleAtPiece { get; set; }
    public bool IsCurveBeginning { get; set; }

    public double Length;
    public bool? Switch;
    public int? Radius;
    public double? Angle;

    //Empty Constructor
    public Piece() { }

    public bool HasSwitch()
    {
        return this.Switch.HasValue && this.Switch.Value;
    }
}


[Serializable]
public class LaneGi
{

    public int DistanceFromCenter;
    public int Index;

    //Empty Constructor
    public LaneGi() { }

}


[Serializable]
public class Position
{

    public double X;
    public double Y;

    //Empty Constructor
    public Position() { }

}


[Serializable]
public class StartingPoint
{

    public Position Position;
    public double Angle;

    //Empty Constructor
    public StartingPoint() { }

}


[Serializable]
public class Track
{

    public string Id;
    public string Name;
    public Piece[] Pieces;
    public LaneGi[] Lanes;
    public StartingPoint StartingPoint;

    //Empty Constructor
    public Track() { }

}


[Serializable]
public class Dimensions
{

    public double Length;
    public double Width;
    public double GuideFlagPosition;

    //Empty Constructor
    public Dimensions() { }

}


[Serializable]
public class Car
{

    public Id Id;
    public Dimensions Dimensions;

    //Empty Constructor
    public Car() { }

}


[Serializable]
public class RaceSession
{

    public int Laps;
    public int MaxLapTimeMs;
    public bool QuickRace;

    //Empty Constructor
    public RaceSession() { }

}


[Serializable]
public class Race
{

    public Track Track;
    public Car[] Cars;
    public RaceSession RaceSession;

    //Empty Constructor
    public Race() { }

}


[Serializable]
public class DataGi
{

    public Race Race;

    //Empty Constructor
    public DataGi() { }

}
#endregion

#region YOUR CAR
[Serializable]
public class yourCar
{

    public string MsgType;
    public DataYc Data;

    //Empty Constructor
    public yourCar() { }

    public string Serialize()
    {
        return JsonConvert.SerializeObject(this);
    }
    public static yourCar FromJson(string json)
    {
        return JsonConvert.DeserializeObject<yourCar>(json);
    }
}


[Serializable]
public class DataYc
{

    public string Name;
    public string Color;

    //Empty Constructor
    public DataYc() { }

}
#endregion

#region UTILS
static public class Utils
{
    static public string RaceLogFileName { get; set; }
    static public string SentLogFileName { get; set; }
    static public string ExcelFileName { get; set; }
    static public string VelocityFileName { get; set; }
    static public bool Debug { get; set; }

    static public void Init()
    {
        try
        {
            var hostname = System.Environment.MachineName;
            Debug = hostname == "MSP";
        }
        catch (Exception)
        {

            throw;
        }

        if (Debug)
        {
            Utils.RaceLogFileName = string.Format(@"logs\debug\RaceLog_{0}.txt", "avr");
            Utils.SentLogFileName = string.Format(@"logs\debug\SentLog_{0}.txt", "avr");
            Utils.ExcelFileName = string.Format(@"logs\debug\ExcelRace_{0}.csv", "avr");
            Utils.VelocityFileName = string.Format(@"logs\debug\VelocityLog_{0}.csv", "avr");

            DirectoryInfo di = new DirectoryInfo("logs/debug");
            if (!di.Exists)
            {
                di.Create();
            }
            ClearLogFile(true, true);
        }
    }

    //1,2,3,4,5
    static public List<SwitchAux> ShiftListSwitchs(int carPieceIndex, List<SwitchAux> toShift, int max)
    {
        while (toShift.Count > 1
            && toShift[0].Index < carPieceIndex)
        {
            var shiftedObj = toShift[0];
            toShift.RemoveAt(0);
            toShift.Add(shiftedObj);

            if (toShift[toShift.Count - 1].Index == max)
                break;
        }


        return toShift;
    }

    static public List<Piece> ShiftListPieces(int carPieceIndex, List<Piece> toShift, int nextSwitchIndex, int max)
    {

        while (toShift.Count > 0
            && (toShift[0].ExIndex.Value < carPieceIndex //rotate piece from left to right (in the list), until a beggining curve is found
            || !toShift[0].IsCurveBeginning))
        //&& toShift[0].ExIndex.Value < nextSwitchIndex) //if nextCurve is before next switch, ignore it!
        {
            var shiftedItem = toShift[0];
            toShift.RemoveAt(0);
            toShift.Add(shiftedItem);
            var dbg = string.Format("{0}; toShiftCount:{1};toShift[0].ExIndex.Value:{2}; carPieceIndex:{3}", "ShiftListPiece", toShift[0], toShift[0].ExIndex.Value, carPieceIndex);
            SaveFile(Utils.SentLogFileName, true, dbg);

            if (toShift[toShift.Count - 1].ExIndex.Value == max)
                break;
        }
        return toShift;
    }

    static public void ClearLogFile(bool clearRaceLog, bool clearSentLog)
    {
        if (clearRaceLog)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(RaceLogFileName, false))
            {
                file.Write("");
            }
        }

        if (clearSentLog)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(SentLogFileName, false))
            {
                file.Write("");
            }
        }

        using (System.IO.StreamWriter file = new System.IO.StreamWriter(ExcelFileName, false))
        {
            file.Write("");
        }

        using (System.IO.StreamWriter file = new System.IO.StreamWriter(VelocityFileName, false))
        {
            file.Write("");
        }
    }

    static public string PutQuotes(object obj)
    {
        return string.Format("\"{0}\"", obj);
    }

    static public void SaveFile(string fileName, bool append, string text)
    {
        if (Debug)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileName, append))
            {
                file.WriteLine(text);
            }
        }
    }
}
#endregion

public class SwitchAux
{
    public int Index { get; set; }
    public double[] CostFromLastSwitch { get; set; }
    public double[] TotalCost { get; set; }
}

#region CREATE RACE
[Serializable]
public class createRace
{
    [JsonProperty("msgType")]
    public string MsgType;

    //public string MsgType {get {return "createRace";}}
    [JsonProperty("data")]
    public DataCr Data;

    //Empty Constructor
    public createRace() { }

    public string Serialize()
    {
        return JsonConvert.SerializeObject(this);
    }
    public static createRace FromJson(string json)
    {
        return JsonConvert.DeserializeObject<createRace>(json);
    }
}


[Serializable]
public class BotId
{
    [JsonProperty("name")]
    public string Name;
    [JsonProperty("key")]
    public string Key;

    //Empty Constructor
    public BotId() { }

}


[Serializable]
public class DataCr
{
    [JsonProperty("botId")]
    public BotId BotId;
    [JsonProperty("trackName")]
    public string TrackName;
    [JsonProperty("password")]
    public string Password;
    [JsonProperty("carCount")]
    public int CarCount;

    //Empty Constructor
    public DataCr() { }

}
#endregion

static public class ExtensionMethods
{
    //public const char CSV_TOKEN = ',';
    public const char CSV_TOKEN = ';';

    static public string FormatCsv(this string format, char newChar = CSV_TOKEN, char oldChar = ',')
    {
        return format.Replace(oldChar, newChar);
    }
}
